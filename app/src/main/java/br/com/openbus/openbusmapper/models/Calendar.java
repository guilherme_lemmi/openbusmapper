package br.com.openbus.openbusmapper.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Guilherme on 10/10/2016.
 */
public class Calendar implements Serializable{

    @Expose
    private Long id;

    @Expose
    private String serviceId;

    @Expose
    private boolean monday;

    @Expose
    private boolean tuesday;

    @Expose
    private boolean wednesday;

    @Expose
    private boolean thursday;

    @Expose
    private boolean friday;

    @Expose
    private boolean saturday;

    @Expose
    private boolean sunday;

    public Calendar() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }
}
