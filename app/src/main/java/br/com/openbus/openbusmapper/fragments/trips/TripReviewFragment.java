package br.com.openbus.openbusmapper.fragments.trips;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.openbus.openbusmapper.R;

import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.db.Database;
import br.com.openbus.openbusmapper.models.OLatLng;
import br.com.openbus.openbusmapper.models.SnapToRoadsRequest;
import br.com.openbus.openbusmapper.models.UserTrip;
import br.com.openbus.openbusmapper.network.Api;
import br.com.openbus.openbusmapper.utils.Helper;
import br.com.openbus.openbusmapper.utils.MapUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Guilherme on 13/08/2016.
 */
public class TripReviewFragment extends Fragment implements OnMapReadyCallback{

    @BindView(R.id.trip_headsign)
    TextView tripHeadsign;
    @BindView(R.id.trip_distance)
    TextView tripDistance;
    @BindView(R.id.trip_speed)
    TextView tripSpeed;
    @BindView(R.id.trip_stops)
    TextView tripStops;
    @BindView(R.id.trip_chronometer)
    Chronometer tripChronometer;
    @BindView(R.id.static_map_container)
    FrameLayout staticMapContainer;

    private Unbinder unbinder;

    private GoogleMap staticMap;
    private UserTrip mUserTrip;
    private int stopCounter;
    private View view;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_trip_review, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        if(bundle != null){
            if(bundle.containsKey(MyApplication.USER_TRIP)){
                mUserTrip = (UserTrip) bundle.getSerializable(MyApplication.USER_TRIP);
            }
        }else if(savedInstanceState != null){
            if(savedInstanceState.containsKey(MyApplication.USER_TRIP)){
                mUserTrip = (UserTrip) savedInstanceState.getSerializable(MyApplication.USER_TRIP);
            }
        }

        progressDialog = new ProgressDialog(getActivity());

        setupMapIfNeeded();
        setupUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            if(savedInstanceState.containsKey(MyApplication.USER_TRIP)){
                mUserTrip = (UserTrip) savedInstanceState.getSerializable(MyApplication.USER_TRIP);
            }
        }
    }

    private void setupMapIfNeeded() {
        if (staticMap == null) {
            SupportMapFragment mapFragment =
                    (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.static_map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.v("GLT::","on map ready");
        staticMap = googleMap;
        staticMap.getUiSettings().setScrollGesturesEnabled(false);
        GoogleMapOptions options = new GoogleMapOptions().liteMode(true);
        MapFragment.newInstance(options);

        if(Helper.getConnectivityStatus(getActivity().getApplicationContext()) != Helper.NOT_CONNECTED) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Carregando trajeto...");
            progressDialog.show();

            Call<List<OLatLng>> call = Api.getClient().snapToRoads(
                    Helper.getCurrentToken(getActivity().getApplicationContext()),
                    new SnapToRoadsRequest(mUserTrip.getData()));
            call.enqueue(new Callback<List<OLatLng>>() {
                @Override
                public void onResponse(Response<List<OLatLng>> response, Retrofit retrofit) {
                    dismissProgressBar();
                    if (response.isSuccess()) {
                        List<OLatLng> snappedPoints = response.body();
                        if(snappedPoints.size() > 0){
                            mUserTrip.setData(snappedPoints);
                        }
                    } else {
                        Log.e("GL::","Error snapping to roads");
                    }
                    printTrip();
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                    dismissProgressBar();
                }
            });
        } else {
            printTrip();
        }
    }

    private void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void printTrip(){

        List<LatLng> waypoints = mUserTrip.getDataAsLatLng();
        Polyline line = staticMap.addPolyline(new PolylineOptions()
                .addAll(waypoints)
                .width(5)
                .color(Color.BLUE));

        for(OLatLng stopMarkerPos : mUserTrip.getStopsData()){
            Marker stopMarker = staticMap.addMarker(new MarkerOptions()
                    .position(new LatLng(stopMarkerPos.getLatitude(), stopMarkerPos.getLongitude()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.stop_confirmed))
                    .draggable(false));
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(LatLng latLng : waypoints){
            builder.include(latLng);
        }

        staticMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 17));
        if(mUserTrip.getImgFile() == null){
            staticMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                public void onSnapshotReady(Bitmap bitmap) {
                    // Write image to disk
                    if(bitmap != null && getActivity() != null
                            && bitmap.getWidth() > 0 && bitmap.getHeight() > 0){
                        try {
                            File file = new File(getActivity().getApplicationContext()
                                    .getFilesDir().getPath(),
                                    mUserTrip.getUserTripKey() + ".png");
                            FileOutputStream out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                            mUserTrip.setImgFile(file.getAbsolutePath());

                            Database db = MyApplication.getDb();
                            db.saveUserTrip(getActivity().getApplicationContext(),
                                    mUserTrip.getUserTripKey(), mUserTrip);

                        }catch (FileNotFoundException fnfe){
                            fnfe.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void setupUI(){
        Long duration = mUserTrip.getEnd() - mUserTrip.getStart();
        double speed = mUserTrip.getDistance() / duration;
        tripSpeed.setText((int) MapUtils.convertMpsToKph(speed) + " km/h");
        tripDistance.setText(String.valueOf((int) mUserTrip.getDistance().doubleValue())+" m");
        tripHeadsign.setText(mUserTrip.getTrip().getTripHeadsign());
        tripStops.setText(String.valueOf(mUserTrip.getStopsData().size()));
        SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
        tripChronometer.setText(formatter.format(new Date(duration * 1000)));

        if(mUserTrip != null && mUserTrip.getStopsData().size() > 0){
            tripStops.setText(String.valueOf(mUserTrip.getStopsData().size()));
        }
    }

    public void addStop(){
        stopCounter++;
        tripStops.setText(String.valueOf(stopCounter));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void chronoStart() {
        tripChronometer.setBase( SystemClock.elapsedRealtime() );
        tripChronometer.start();
    }

    private void chronoStop()  {
        tripChronometer.stop();
       // tripChronometer.setBase( SystemClock.elapsedRealtime() );
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(MyApplication.USER_TRIP, mUserTrip);
    }

}
