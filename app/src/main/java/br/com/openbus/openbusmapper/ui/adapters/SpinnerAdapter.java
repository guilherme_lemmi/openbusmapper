package br.com.openbus.openbusmapper.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guilherme on 10/10/2016.
 */
public class SpinnerAdapter<T> extends ArrayAdapter {
    private List<Integer> disabled = new ArrayList<>();
    public SpinnerAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
    }
    public void setDisabled(List<Integer> disabled) {
        this.disabled = disabled;
    }
    @Override
    public boolean isEnabled(int position) {
        return disabled==null || !disabled.contains(position);
    }
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        } else {
            view = convertView;
        }
        TextView tv = (TextView) view.findViewById(android.R.id.text1);
        tv.setText((String) getItem(position));
        tv.setTextColor(isEnabled(position) ? Color.BLACK : Color.GRAY);
        return view;
    }
}

