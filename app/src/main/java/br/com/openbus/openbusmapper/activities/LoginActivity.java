package br.com.openbus.openbusmapper.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.fragments.login.LoginFragment;
import br.com.openbus.openbusmapper.fragments.login.RegisterFragment;
import br.com.openbus.openbusmapper.ui.WrappedViewPager;
import br.com.openbus.openbusmapper.utils.Helper;

/**
 * Created by Guilherme on 15/08/2016.
 */
public class LoginActivity extends AppCompatActivity {

    private static final int NUM_PAGES = 2;
    private WrappedViewPager mPager;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        boolean expiredToken = getIntent().getBooleanExtra(MyApplication.TOKEN_EXPIRED, false);
        if(expiredToken){
            View view = this.findViewById(android.R.id.content); //?
            if(view != null){
                Helper.showMsg(this,getString(R.string.expired_token), true);
            }
        }

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (WrappedViewPager) findViewById(R.id.pager);
        mPagerAdapter = new LoginPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    private class LoginPagerAdapter extends FragmentStatePagerAdapter {
        public LoginPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return new LoginFragment();
                case 1: return new RegisterFragment();
                default: return new LoginFragment();
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public WrappedViewPager getPager() {
        return mPager;
    }
}

