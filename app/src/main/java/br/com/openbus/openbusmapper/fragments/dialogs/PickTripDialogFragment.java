package br.com.openbus.openbusmapper.fragments.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.db.Database;
import br.com.openbus.openbusmapper.listeners.OnTripPickedListener;
import br.com.openbus.openbusmapper.models.Campaign;
import br.com.openbus.openbusmapper.models.Trip;
import br.com.openbus.openbusmapper.network.Api;
import br.com.openbus.openbusmapper.network.RestCallback;
import br.com.openbus.openbusmapper.ui.adapters.SpinnerAdapter;
import br.com.openbus.openbusmapper.ui.adapters.TripAdapter;
import br.com.openbus.openbusmapper.utils.Helper;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Guilherme on 25/09/2016.
 */
public class PickTripDialogFragment extends AppCompatDialogFragment {

    public static final String TAG = PickTripDialogFragment.class.getSimpleName();

//    @BindView(R.id.campaign_spinner)
//    AppCompatSpinner campaignSpinner;
    @BindView(R.id.trip_pick_list)
    RecyclerView tripsListView;
    @BindView(R.id.search_trips)
    EditText searchTripsInput;
    @BindView(R.id.load_trips_progress)
    ContentLoadingProgressBar contentLoadingProgressBar;

    private View mView;
    private Trip mSelectedTrip;
    private Long selectedTripId = null;
    private Campaign mSelectedCampaign;
    private List<Campaign> mCampaignsList = new ArrayList<>();
    private List<String> mCampaignsNamesList = new ArrayList<>();
    private List<Trip> mTripsList = new ArrayList<>();
    private List<Trip> mAllTrips = new ArrayList<>();
    private SpinnerAdapter mCampaignAdapter;
    private TripAdapter mTripAdapter;

    private OnTripPickedListener onTripPickedListener;

    public void setOnTripPickedListener(OnTripPickedListener onTripPickedListener) {
        this.onTripPickedListener = onTripPickedListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ViewGroup nullParent = null;
        mView =  View.inflate(getContext(), R.layout.dialog_pick_trip, null);
//                getActivity().getLayoutInflater().inflate(R.layout.dialog_pick_trip, nullParent, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.action_record_trip)
                .setView(mView)
                .setPositiveButton(R.string.action_ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                //leave empty, gets overriden on setOnShowListener
                            }
                        }
                )
                .setNegativeButton(R.string.action_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
                                dismiss();
                            }
                        }
                );

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button positiveButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        if(onTripPickedListener != null){
                            onTripPickedListener.onTripPicked(mTripAdapter.getSelectedTrip(),
                                    mSelectedCampaign);
                        }
                        dismiss();
                    }
                });
            }
        });

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        if(mView == null)
            mView = inflater.inflate(R.layout.dialog_pick_trip, container, false);
        ButterKnife.bind(this, mView);

//        campaignSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                String campaignName = String.valueOf(mCampaignAdapter.getItem(position));
//                Campaign selectedCampaign = null;
//                for(Campaign campaign : mCampaignsList){
//                    if(campaign.toString().equals(campaignName)){
//                        selectedCampaign = campaign;
//                        break;
//                    }
//                }
//                refreshTripList(selectedCampaign);
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) { }
//        });

        mTripsList = new ArrayList<>();
        mAllTrips = new ArrayList<>();
        mTripAdapter = new TripAdapter(getContext(), mTripsList);
        tripsListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        tripsListView.setAdapter(mTripAdapter);
        loadTrips();

        searchTripsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final List<Trip> filteredModelList = filter(mAllTrips, s.toString());
                mTripAdapter.animateTo(filteredModelList);
                tripsListView.scrollToPosition(0);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return mView;
    }

    private List<Trip> filter(List<Trip> models, String query) {
        query = query.toLowerCase();
        final List<Trip> filteredModelList = new ArrayList<>();
        for (Trip model : models) {
            final String text = model.getTripHeadsign().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void loadTrips(){

        if(Helper.getConnectivityStatus(getActivity().getApplicationContext()) == Helper.NOT_CONNECTED){
            loadCachedCampaigns();
            contentLoadingProgressBar.setVisibility(View.GONE);
        }else{
            final Call<List<Campaign>> call = Api.getClient().getCampaigns(
                    Helper.getCurrentToken(getActivity().getApplicationContext())
            );
            call.enqueue(new RestCallback<List<Campaign>>(getActivity()) {
                @Override
                public void onResponse(Response<List<Campaign>> response, Retrofit retrofit) {
                    super.onResponse(response, retrofit);
                    contentLoadingProgressBar.setVisibility(View.GONE);

                    if (response.isSuccess() && getActivity() != null) {
                        List<Campaign> campaigns = response.body();
                        mSelectedCampaign = campaigns.size() > 0 ? campaigns.get(0) : null;
                        mCampaignsList.addAll(campaigns);
                        Database db = MyApplication.getDb();
                        String userKey = "user_campaigns:"+Helper.getCurrentUserId(getActivity().getApplicationContext());
                        for(Campaign campaign : campaigns){
                            String campaignKey = userKey + ":" + String.format("%05d", campaign.getId());
                            db.saveCampaign(getActivity().getApplicationContext(),
                                    campaignKey,
                                    campaign);
                            mCampaignsNamesList.add(campaign.toString());
                        }

                        if(mCampaignsNamesList.size() > 0){
                            refreshTripList(campaigns.get(0));
                        }

                    } else {
                        Log.v(TAG,"Error loading trips: "+response.code());
                        loadCachedCampaigns();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    loadCachedCampaigns();
                    contentLoadingProgressBar.setVisibility(View.GONE);
                    t.printStackTrace();
                }
            });
        }
    }

    private void loadCachedCampaigns(){
        Database db = MyApplication.getDb();
        List<Campaign> campaigns = db.getCampaigns(getActivity().getApplicationContext(),
                String.valueOf(Helper.getCurrentUserId(getActivity().getApplicationContext())));

        if(campaigns.size() > 0){
            mSelectedCampaign = campaigns.size() > 0 ? campaigns.get(0) : null;
            mCampaignsList.addAll(campaigns);
            for(Campaign campaign : campaigns){
                mCampaignsNamesList.add(campaign.toString());
            }
            refreshTripList(mSelectedCampaign);
        }
    }

    private void refreshTripList(Campaign campaign){
        if(campaign != null){
            mTripsList.clear();
            mAllTrips.clear();
            mTripsList.addAll(campaign.getTrips());
            mAllTrips.addAll(campaign.getTrips());
            mTripAdapter.notifyDataSetChanged();
        }
    }

}
