package br.com.openbus.openbusmapper.db;

/**
 * Created by Guilherme on 25/10/2016.
 */

import android.content.Context;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappyDB;
import com.snappydb.SnappydbException;

import java.util.ArrayList;
import java.util.List;

import br.com.openbus.openbusmapper.models.Campaign;
import br.com.openbus.openbusmapper.models.UserTrip;

@SuppressWarnings("SynchronizeOnNonFinalField")
public final class Database {

    private DB mSnappyDb;
    private String mDbName;

    public Database(Context context, String dbName) {
        this.mDbName = dbName;
        try {
            mSnappyDb = new SnappyDB.Builder(context)
                    .name(dbName)
                    .build();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public Campaign saveCampaign(Context context, String key, Campaign campaign) {
        synchronized (mSnappyDb) {
            try {
                mSnappyDb = DBFactory.open(context, mDbName);
                mSnappyDb.put(key, campaign);
                mSnappyDb.close();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            return campaign;
        }
    }

    public List<Campaign> getCampaigns(Context context, String userId) {
        synchronized (mSnappyDb) {
            ArrayList<Campaign> campaigns = new ArrayList<>();
            try {
                mSnappyDb = DBFactory.open(context, mDbName);
                for (String key : mSnappyDb.findKeys("user_campaigns:"+userId+":")) {
                    campaigns.add(mSnappyDb.getObject(key, Campaign.class));
                }
                mSnappyDb.close();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            return campaigns;
        }
    }

    public List<UserTrip> getUsertrips(Context context, String userId) {
        synchronized (mSnappyDb) {
            ArrayList<UserTrip> userTrips = new ArrayList<>();
            try {
                mSnappyDb = DBFactory.open(context, mDbName);
                for (String key : mSnappyDb.findKeys("user_trips:"+userId+":")) {
                    userTrips.add(mSnappyDb.getObject(key, UserTrip.class));
                }
                mSnappyDb.close();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            return userTrips;
        }
    }

    public UserTrip getUsertrip(Context context, String key) {
        synchronized (mSnappyDb) {
            UserTrip userTrip = null;
            try {
                mSnappyDb = DBFactory.open(context, mDbName);
                userTrip = mSnappyDb.getObject(key, UserTrip.class);
                mSnappyDb.close();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            return userTrip;
        }
    }

    public UserTrip saveUserTrip(Context context, String key, UserTrip userTrip) {
        synchronized (mSnappyDb) {
            try {
                mSnappyDb = DBFactory.open(context, mDbName);
                mSnappyDb.put(key, userTrip);
                mSnappyDb.close();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            return userTrip;
        }
    }

    public String[] findKeys(Context context, String key) {
        synchronized (mSnappyDb) {
            try {
                mSnappyDb = DBFactory.open(context, mDbName);
                String[] keys = mSnappyDb.findKeys(key);
                mSnappyDb.close();
                return keys;
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
/*
    public boolean isEmpty() {
        synchronized (snappydb) {
            try {
                int nbCat = snappydb.countKeys("category:");
                return nbCat < 1;
            } catch (SnappydbException e) {
                e.printStackTrace();
                throw new IllegalStateException(e);
            }
        }
    }

    @Override
    public void deleteCategories() {
        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("category:")) {
                    snappydb.del(key);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
    }

    @Override
    public void deleteVenues() {
        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("tag:")) {
                    snappydb.del(key);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }
    }
    */

}
