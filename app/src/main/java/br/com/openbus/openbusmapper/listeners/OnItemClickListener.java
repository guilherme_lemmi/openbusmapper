package br.com.openbus.openbusmapper.listeners;

import android.view.View;

/**
 * Created by Guilherme on 25/09/2016.
 */
public interface OnItemClickListener {

    void onClick(View view, int position, boolean isLongClick);
}
