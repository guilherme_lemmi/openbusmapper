package br.com.openbus.openbusmapper.activities;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.db.Database;
import br.com.openbus.openbusmapper.models.UserTrip;
import br.com.openbus.openbusmapper.ui.adapters.UserTripAdapter;
import br.com.openbus.openbusmapper.utils.Helper;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Guilherme on 15/10/2016.
 */
public class MyTripsActivity extends AppCompatActivity {

    @BindView(R.id.trips_list)
    RecyclerView tripsListView;

    private List<UserTrip> mUserTrips = new ArrayList<>();
    private UserTripAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trips);
        ButterKnife.bind(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Meus Trajetos");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        loadUserTrips();
    }

    private void loadUserTrips(){

        Database db = MyApplication.getDb();
        mUserTrips = db.getUsertrips(getApplicationContext(),
                String.valueOf(Helper.getCurrentUserId(getApplicationContext())));

        mAdapter = new UserTripAdapter(this, mUserTrips);
        tripsListView.setLayoutManager(new LinearLayoutManager(this));
        tripsListView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
