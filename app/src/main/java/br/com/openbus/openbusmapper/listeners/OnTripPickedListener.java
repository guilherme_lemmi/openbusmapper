package br.com.openbus.openbusmapper.listeners;

import br.com.openbus.openbusmapper.models.Campaign;
import br.com.openbus.openbusmapper.models.Trip;

/**
 * Created by Guilherme on 25/09/2016.
 */
public interface OnTripPickedListener {
    void onTripPicked(Trip trip, Campaign campaign);
}
