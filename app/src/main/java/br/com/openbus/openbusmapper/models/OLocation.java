package br.com.openbus.openbusmapper.models;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Guilherme on 16/10/2016.
 */
public class OLocation implements Parcelable, Serializable{

    @Expose
    private String provider;

    @Expose
    private long time = 0;

    @Expose
    private double latitude = 0.0;

    @Expose
    private double longitude = 0.0;

    @Expose
    private double altitude = 0.0f;

    @Expose
    private float speed = 0.0f;

    @Expose
    private float bearing = 0.0f;

    @Expose
    private float accuracy = 0.0f;

    @Expose
    private boolean is_stop = false;

    public OLocation() {
    }

    public OLocation(Location location){
        this.accuracy = location.getAccuracy();
        this.provider = location.getProvider();
        this.time = location.getTime();
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.altitude = location.getAltitude();
        this.speed = location.getSpeed();
        this.bearing = location.getBearing();
        this.accuracy = location.getAccuracy();
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isStop() {
        return is_stop;
    }

    public void setIsStop(boolean is_stop) {
        this.is_stop = is_stop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public OLocation(Parcel in){
        this.provider = in.readString();
        this.time = in.readLong();

        double[] data = new double[3];
        in.readDoubleArray(data);
        this.latitude = data[0];
        this.longitude = data[1];
        this.altitude = data[2];

        float[] floatData = new float[3];
        in.readFloatArray(floatData);
        this.speed = floatData[0];
        this.bearing = floatData[1];
        this.accuracy = floatData[2];
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.provider);
        dest.writeLong(this.time);
        dest.writeDoubleArray(new double[] {this.latitude, this.longitude, this.getAltitude()});
        dest.writeFloatArray(new float[] {this.speed, this.bearing, this.accuracy});
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public OLocation createFromParcel(Parcel in) {
            return new OLocation(in);
        }

        public OLocation[] newArray(int size) {
            return new OLocation[size];
        }
    };
}
