package br.com.openbus.openbusmapper.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;

import java.util.List;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.activities.LoginActivity;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.models.Campaign;

/**
 * Created by Guilherme on 09/10/2016.
 */
public class Helper {

    public static final String TAG = Helper.class.getSimpleName();
    public static final int WIFI = 1;
    public static final int MOBILE = 2;
    public static final int NOT_CONNECTED = 0;

    public static String getCurrentToken(Context context){
        SharedPreferences pref = context.getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE);
        return pref.getString(MyApplication.TOKEN, null);
    }

    public static Long getCurrentUserId(Context context){
        SharedPreferences pref = context.getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE);
        return pref.getLong(MyApplication.USER_ID, 0);
    }

    public static String getCurrentUserEmail(Context context){
        SharedPreferences pref = context.getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE);
        return pref.getString(MyApplication.USER_EMAIL, null);
    }

    public static String getCurrentUserAvatar(Context context){
        SharedPreferences pref = context.getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE);
        return pref.getString(MyApplication.USER_AVATAR, null);
    }

    public static String getUserCampaigns(Context context){
        SharedPreferences pref = context.getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE);
        String campaigns = pref.getString(MyApplication.USER_CAMPAIGNS, null);
        Log.v("GL::","load offline campigns : "+campaigns);
        return campaigns;
    }

    public static void setUserCampaigns(Context context, String campaigns){
        SharedPreferences.Editor editor = context
                .getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE)
                .edit();
        Log.v("GL::","set offline campigns : "+campaigns);
        editor.putString(MyApplication.USER_CAMPAIGNS, campaigns);
        editor.apply();
    }

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return MOBILE;
        }
        return NOT_CONNECTED;
    }

    public static void logout(Activity activity) {
        logout(activity, false);
    }

    public static void logout(Activity activity, boolean expiredToken) {

        if(AccessToken.getCurrentAccessToken() != null){
            LoginManager.getInstance().logOut(); //facebook logout
        }

        SharedPreferences sharedPref = activity.getApplicationContext().getSharedPreferences(
                MyApplication.USER, Context.MODE_PRIVATE);
        sharedPref.edit().clear().apply();

        Intent i = new Intent(activity, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        if(expiredToken){
            i.putExtra(MyApplication.TOKEN_EXPIRED, true);
        }

        activity.finish();
        activity.startActivity(i);
    }

    public static void notSignedIn(final Context context) {
        new AlertDialog.Builder(context)
                .setMessage("Necessário estar logado. Deseja ir para a tela de login?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i =  new Intent(context, LoginActivity.class);
                        context.startActivity(i);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public static void showMsg(Activity activity, String errorMsg, boolean isError) {
        if(activity!=null){
            View view = activity.findViewById(android.R.id.content);
            String errorTxt = errorMsg!=null ? errorMsg : activity.getString(R.string.generic_failure_error);
            Snackbar.make(view, errorTxt, Snackbar.LENGTH_LONG)
                    .setAction(activity.getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    }).setActionTextColor(isError ? Color.RED : Color.WHITE).show();
        }
    }

}

