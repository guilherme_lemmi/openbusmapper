package br.com.openbus.openbusmapper.fragments.login;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.activities.LoginActivity;
import br.com.openbus.openbusmapper.activities.MainActivity;
import br.com.openbus.openbusmapper.network.Api;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.utils.Helper;
import br.com.openbus.openbusmapper.models.User;
import butterknife.BindView;
import butterknife.ButterKnife;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Guilherme on 22/05/2016.
 */
public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.login_password)
    AppCompatEditText userPasswordField;
    @BindView(R.id.login_email)
    AppCompatEditText userEmailField;
    @BindView(R.id.btn_login)
    Button loginBtn;
    @BindView(R.id.new_account_link)
    TextView newAccountLink;
    @BindView(R.id.anonymous_link)
    TextView anonymousLink;

    private View mView;
    private String mEmail;

    private ProgressDialog progressDialog;

    CallbackManager callbackManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, mView);

        progressDialog = new ProgressDialog(getActivity());

        loginBtn.setOnClickListener(mLoginListener);

        LoginButton loginButton = (LoginButton) mView.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email","user_friends"));
        loginButton.setFragment(this);
        // Callback registration
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("GL::",object.toString());
                                try{
                                    String id = object.has("id") ? object.getString("id") : null;
                                    facebookLogin(AccessToken.getCurrentAccessToken().getToken(),
                                            id);
                                }catch(JSONException je){
                                    je.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,age_range,picture");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        newAccountLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivity) getActivity()).getPager().setCurrentItem(1);
            }
        });

        anonymousLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<User> call = Api.getClient().registerAnonymous();
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Response<User> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            // get authentication token
                            final String token = response.headers().get(MyApplication.TOKEN_HEADER);
                            User user = response.body();
                            finishLogin(token, user);

                        } else {
                            ResponseBody responseInfo = response.errorBody();
                            // handleApiError(response.code(), responseInfo);
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        t.printStackTrace();
                        //  onRequestFailed(null);
                    }
                });
            }
        });

        return mView;
    }

    private View.OnClickListener mLoginListener = new View.OnClickListener() {
        public void onClick(View v) {

            // Check if no view has focus
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                // close keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            boolean valid = true;
            View focusView = null;
            String errorMsg = null;

            String email = userEmailField.getText().toString();
            String password = userPasswordField.getText().toString();

            if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                valid = false;
                if(focusView==null) {
                    focusView = userEmailField;
                    errorMsg = getString(R.string.email_error);
                }
                userEmailField.setError(getString(R.string.email_error));
            }else{
                userEmailField.setError(null);
            }

            if (password.isEmpty() || password.length() < 4){
                valid = false;
                String msg = password.isEmpty() ? getString(R.string.empty_password_error) : getString(R.string.short_password_error);
                if(focusView==null) {
                    focusView = userPasswordField;
                    errorMsg = msg;
                }
                userPasswordField.setError(msg);

            } else {
                userPasswordField.setError(null);
            }

            if (valid) {
                loginBtn.setEnabled(false);

                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(getString(R.string.authenticating));
                progressDialog.show();

                // Send authentication info to the server
                if (Helper.getConnectivityStatus(getContext()) != Helper.NOT_CONNECTED) {

                    Call<User> call = Api.getClient().login(email, password);
                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Response<User> response, Retrofit retrofit) {
                            dismissProgressBar();
                            Log.v(TAG,"response is "+ response.raw());

                            if (response.isSuccess()) {
                                // get authentication token
                                final String token = response.headers().get(MyApplication.TOKEN_HEADER);
                                User user = response.body();
                                finishLogin(token, user);

                            } else {
                                Log.e(TAG, "Error code: " + response.code());
                                ResponseBody responseInfo = response.errorBody();
                                // handleApiError(response.code(), responseInfo);
                            }

                        }

                        @Override
                        public void onFailure(Throwable t) {
                            t.printStackTrace();
                            dismissProgressBar();
                            //  onRequestFailed(null);
                        }
                    });

                } else {
                    // onRequestFailed(getString(R.string.no_internet));
                }

            } else {
                //  onRequestFailed(errorMsg);
                if (focusView != null)
                    focusView.requestFocus();
                return;
            }
        }
    };

    public void finishLogin(String token, User user){
        //Save the token
        SharedPreferences.Editor editor = getContext()
                .getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE)
                .edit();
        Log.v("GL::","Bearer " + token);
        editor.putString(MyApplication.TOKEN, "Bearer " + token);
        editor.putLong(MyApplication.USER_ID, user.getId());
        editor.putString(MyApplication.USER_AVATAR, user.getPicture());
        editor.putString(MyApplication.USER_EMAIL, user.getEmail());
        editor.apply();

        dismissProgressBar();

        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissProgressBar();
    }

    public void facebookLogin(final String fbToken, String fbUid){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.authenticating));
        progressDialog.show();
        if (Helper.getConnectivityStatus(getContext()) != Helper.NOT_CONNECTED) {
            Log.v("GL::","fbtoken is "+fbToken);
            Call<User> call = Api.getClient().facebookLogin(
                    fbToken,
                    fbUid
            );
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Response<User> response, Retrofit retrofit) {
                    dismissProgressBar();
                    if (response.isSuccess()) {
                        final String token = response.headers().get(MyApplication.TOKEN_HEADER);
                        User user = response.body();
                        finishLogin(token, user);
                    } else {
                        Log.e(TAG, "Error code: " + response.code());
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
        }else{
            Log.e(TAG, "no internet!");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
