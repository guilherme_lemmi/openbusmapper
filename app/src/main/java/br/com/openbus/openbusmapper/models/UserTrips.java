package br.com.openbus.openbusmapper.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guilherme on 15/10/2016.
 */
public class UserTrips {

    private List<UserTrip> userTrips = new ArrayList<>();

    public UserTrips() {
    }

    public List<UserTrip> getUserTrips() {
        return userTrips;
    }

    public void setUserTrips(List<UserTrip> userTrips) {
        this.userTrips = userTrips;
    }

    public void addTrip(UserTrip trip){
        userTrips.add(trip);
    }
}
