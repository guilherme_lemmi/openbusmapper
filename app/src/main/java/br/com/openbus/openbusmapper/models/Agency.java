package br.com.openbus.openbusmapper.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Guilherme on 10/10/2016.
 */
public class Agency implements Serializable{

    @Expose
    private Long id;

    @Expose
    private String agencyName;

    @Expose
    private String agencyUrl;

    @Expose
    private String agencyPhone;

    public Agency() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgencyUrl() {
        return agencyUrl;
    }

    public void setAgencyUrl(String agencyUrl) {
        this.agencyUrl = agencyUrl;
    }

    public String getAgencyPhone() {
        return agencyPhone;
    }

    public void setAgencyPhone(String agencyPhone) {
        this.agencyPhone = agencyPhone;
    }
}
