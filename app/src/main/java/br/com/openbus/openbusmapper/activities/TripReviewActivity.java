package br.com.openbus.openbusmapper.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.db.Database;
import br.com.openbus.openbusmapper.fragments.trips.TripReviewFragment;
import br.com.openbus.openbusmapper.models.UserTrip;
import br.com.openbus.openbusmapper.network.Api;
import br.com.openbus.openbusmapper.network.RestCallback;
import br.com.openbus.openbusmapper.utils.Helper;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Guilherme on 15/10/2016.
 */
public class TripReviewActivity extends AppCompatActivity {

    private static final String TRIP_REVIEW_FRAGMENT = "TRIP_REVIEW_FRAGMENT";
    private String mUserTripKey;
    private UserTrip mUserTrip;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_review);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mUserTripKey = getIntent().getStringExtra(MyApplication.USER_TRIP_KEY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mUserTripKey != null){
            loadTrip();
        }
    }

    private void loadTrip(){
        Database db = MyApplication.getDb();
        mUserTrip = db.getUsertrip(getApplicationContext(), mUserTripKey);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mUserTrip.getTrip().getTripHeadsign());
        }

        FragmentManager fm = getSupportFragmentManager();
        TripReviewFragment mTripReviewFragment = new TripReviewFragment();
        Bundle args = new Bundle();
        args.putSerializable(MyApplication.USER_TRIP, mUserTrip);
        mTripReviewFragment.setArguments(args);
        fm.beginTransaction()
                .add(R.id.trip_review_container,
                        mTripReviewFragment, TRIP_REVIEW_FRAGMENT).commitNow();

    }

    private void sendUserTrip(){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Enviando trajeto...");
        progressDialog.show();

        Call<UserTrip> call = Api.getClient().sendUserTrip(
                Helper.getCurrentToken(getApplicationContext()),
                mUserTrip
        );
        call.enqueue(new RestCallback<UserTrip>(this) {
            @Override
            public void onResponse(Response<UserTrip> response, Retrofit retrofit) {
                super.onResponse(response, retrofit);
                dismissProgressBar();
                if (response.isSuccess()) {
                    mUserTrip.setSynched(true);
                    Database db = MyApplication.getDb();
                    db.saveUserTrip(getApplicationContext(), mUserTripKey, mUserTrip);
                    Helper.showMsg(TripReviewActivity.this, "Trajeto enviado com sucesso!", false);
                } else {
                    dismissProgressBar();
                    Helper.showMsg(TripReviewActivity.this, "Erro ao enviar trajeto!", true);
                    Log.e("TripReviewActivity", "Error code: " + response.code());
                }
            }
            @Override
            public void onFailure(Throwable t) {
                dismissProgressBar();
                Helper.showMsg(TripReviewActivity.this, "Erro ao enviar trajeto!", true);
                t.printStackTrace();
            }
        });
    }

    private void dismissProgressBar(){
        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.review_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send:
                if(mUserTrip.getSynched()){
                    Helper.showMsg(this, "Você já enviou esse trajeto!", false);
                }else{
                    if(Helper.getConnectivityStatus(this) == Helper.NOT_CONNECTED){
                        Helper.showMsg(this, "Você não está conectado", true);
                    }else if(mUserTrip != null && !mUserTrip.getSynched()){
                        sendUserTrip();
                    }
                }
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
