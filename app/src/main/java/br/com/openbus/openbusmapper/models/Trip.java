package br.com.openbus.openbusmapper.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Guilherme on 10/10/2016.
 */
public class Trip implements Serializable{

    @Expose
    private Long id;

    @Expose
    private Calendar calendar;

    @Expose
    @SerializedName("trip_headsign")
    private String tripHeadsign;

    @Expose
    private String tripShortName;

    @Expose
    private boolean direction;

    @Expose
    private String blockId;

    @Expose
    private Integer wheelchairAccessible;

    @Expose
    @SerializedName("headsign_return")
    private String headsignReturn;

    @Expose
    private String via;

    @Expose
    private Route route;

    public Trip() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public String getTripHeadsign() {
        return tripHeadsign;
    }

    public void setTripHeadsign(String tripHeadsign) {
        this.tripHeadsign = tripHeadsign;
    }

    public String getTripShortName() {
        return tripShortName;
    }

    public void setTripShortName(String tripShortName) {
        this.tripShortName = tripShortName;
    }

    public boolean isDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public Integer getWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public void setWheelchairAccessible(Integer wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
    }

    public String getHeadsignReturn() {
        return headsignReturn;
    }

    public void setHeadsignReturn(String headsignReturn) {
        this.headsignReturn = headsignReturn;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
