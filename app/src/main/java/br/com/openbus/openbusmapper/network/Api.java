package br.com.openbus.openbusmapper.network;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.openbus.openbusmapper.models.Campaign;
import br.com.openbus.openbusmapper.models.Line;
import br.com.openbus.openbusmapper.models.OLatLng;
import br.com.openbus.openbusmapper.models.SnapToRoadsRequest;
import br.com.openbus.openbusmapper.models.User;
import br.com.openbus.openbusmapper.models.UserTrip;
import butterknife.BindView;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Guilherme on 09/10/2016.
 */
public class Api {

    private static ApiClient apiClient;
    private static final long CONNECT_TIMEOUT_MILLIS = 5000;
    private static final long READ_TIMEOUT_MILLIS = 15000;

    //TODO: update when in production:
//    public static final String baseUrl = "http://api.openbus.local.com.br/";
//    public static final String baseUrl = "http://api.openbus.com.br/";
    public static final String baseUrl = "http://192.168.0.80/";

    public static ApiClient getClient() {
        if (apiClient == null) {
            OkHttpClient httpClient = new OkHttpClient();
            httpClient.setConnectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
            httpClient.setReadTimeout(READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.interceptors().add(logging);

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            apiClient = client.create(ApiClient.class);
        }
        return apiClient;
    }

    public interface ApiClient {

        /* User */
        @POST("user/login")
        @FormUrlEncoded
        Call<User> login(
                @Field("username")     String email,
                @Field("password")  String password
        );

        @POST("user/anonymous")
        Call<User> registerAnonymous();

        @POST("user/register")
        @FormUrlEncoded
        Call<User> register(
                @Field("name")      String name,
                @Field("email")     String email,
                @Field("password")  String password
        );

        @POST("user/fblogin")
        @FormUrlEncoded
        Call<User> facebookLogin(
                @Field("fb_token")   String fbToken,
                @Field("fb_uid")     String fbId
        );

        @POST("user")
        @Headers("Content-Type: application/json")
        Call<User> updateUser(
                @Header("Authorization") String token,
                @Body User user
        );

        @GET("user/{id}")
        Call<User> getCurrentUser(
                @Header("Authorization") String token,
                @Path("id") Long id);

        @POST("user/passwordReset")
        @FormUrlEncoded
        Call<Void> forgotPassword(@Field("email") String email);


        /* Lines */
        @GET("lines")
        @Headers("Content-Type: application/json")
        Call<List<Line>> getLines(
                @Header("Authorization") String token
        );

        /* Campaign */
        @GET("campaign?expand=user,trips")
        @Headers("Content-Type: application/json")
        Call<List<Campaign>> getCampaigns(
                @Header("Authorization") String token
        );

        /* Campaign */
        @GET("campaign/{id}?expand=user,trips")
        @Headers("Content-Type: application/json")
        Call<List<Campaign>> getCampaign(
                @Header("Authorization") String token,
                @Path("id") Long campaignId
        );

        /* Usertrips */
        @POST("usertrip")
        @Headers("Content-Type: application/json")
        Call<UserTrip> sendUserTrip(
                @Header("Authorization") String token,
                @Body UserTrip userTrip
        );

        @POST("usertrip/snap")
        @Headers("Content-Type: application/json")
        Call<List<OLatLng>> snapToRoads(
                @Header("Authorization") String token,
                @Body SnapToRoadsRequest request
        );

    }
}

