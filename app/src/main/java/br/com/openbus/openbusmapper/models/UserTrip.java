package br.com.openbus.openbusmapper.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guilherme on 15/10/2016.
 */
public class UserTrip implements Serializable{

    @Expose
    private Long id;

    @Expose
    private Trip trip;

    @Expose
    private User user;

    @Expose
    private List<OLocation> rawData = new ArrayList<>();

    @Expose
    private List<OLatLng> data = new ArrayList<>();

    @Expose
    private List<OLatLng> stopsData = new ArrayList<>();

    @Expose
    private Boolean synched = false;

    @Expose
    private Long start;

    @Expose
    private Long end;

    private Double distance = 0D;

    private Double lastDistance = 0D;

    private Long lastTime = 0L;

    private Campaign campaign;

    private String userTripKey;

    private String imgFile;

    public UserTrip() {
    }

    public UserTrip(Trip trip, User user) {
        this.trip = trip;
        this.user = user;
    }

    public UserTrip(Trip trip, User user, Campaign campaign) {
        this.trip = trip;
        this.user = user;
        this.campaign = campaign;
    }

    public UserTrip(Trip trip) {
        this.trip = trip;
    }

    public List<OLatLng> getData() {
        return data;
    }

    public ArrayList<LatLng> getDataAsLatLng(){
        ArrayList<LatLng> latLngs = new ArrayList<>();
        for (OLatLng latLng : data){
            latLngs.add(new LatLng(latLng.getLatitude(),latLng.getLongitude()));
        }
        return latLngs;
    }

    public void setData(List<OLatLng> data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<OLocation> getRawData() {
        return rawData;
    }

    public void setRawData(List<OLocation> rawData) {
        this.rawData = rawData;
    }

    public Boolean getSynched() {
        return synched;
    }

    public void setSynched(Boolean synched) {
        this.synched = synched;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<OLatLng> getStopsData() {
        return stopsData;
    }

    public void setStopsData(List<OLatLng> stopsData) {
        this.stopsData = stopsData;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public String getImgFile() {
        return imgFile;
    }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public String getUserTripKey() {
        return userTripKey;
    }

    public void setUserTripKey(String userTripKey) {
        this.userTripKey = userTripKey;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getLastDistance() {
        return lastDistance;
    }

    public void setLastDistance(Double lastDistance) {
        this.lastDistance = lastDistance;
    }

    public Long getLastTime() {
        return lastTime;
    }

    public void setLastTime(Long lastTime) {
        this.lastTime = lastTime;
    }
}
