package br.com.openbus.openbusmapper.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Guilherme on 10/10/2016.
 */
public class Campaign implements Serializable{

    @Expose
    private Long id;

    @Expose
    private String name;

    @Expose
    private String description;

    @Expose
    private String city;

    @Expose
    private boolean isOpen;

    @Expose
    private boolean colaboratorRoutes;

    @Expose
    private boolean colaboratorInvites;

    @Expose
    private String createdAt;

    @Expose
    private User user;

    @Expose
    private List<Trip> trips;

    public Campaign() {
    }

    @Override
    public String toString() {
        return id + " - " + name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isColaboratorRoutes() {
        return colaboratorRoutes;
    }

    public void setColaboratorRoutes(boolean colaboratorRoutes) {
        this.colaboratorRoutes = colaboratorRoutes;
    }

    public boolean isColaboratorInvites() {
        return colaboratorInvites;
    }

    public void setColaboratorInvites(boolean colaboratorInvites) {
        this.colaboratorInvites = colaboratorInvites;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}

