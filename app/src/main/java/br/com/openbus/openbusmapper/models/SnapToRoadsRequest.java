package br.com.openbus.openbusmapper.models;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Guilherme on 25/10/2016.
 */
public class SnapToRoadsRequest {
    @Expose
    private List<OLatLng> locations;

    public SnapToRoadsRequest(List<OLatLng> locations) {
        this.locations = locations;
    }
}
