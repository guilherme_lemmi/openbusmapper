package br.com.openbus.openbusmapper.fragments.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.squareup.okhttp.ResponseBody;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.activities.MainActivity;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.models.User;
import br.com.openbus.openbusmapper.network.Api;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Guilherme on 22/05/2016.
 */
public class RegisterFragment extends Fragment {

    @BindView(R.id.register_name)
    AppCompatEditText registerName;
    @BindView(R.id.register_email)
    AppCompatEditText registerEmail;
    @BindView(R.id.register_password)
    AppCompatEditText registerPassword;
    @BindView(R.id.register_password_confirmation)
    AppCompatEditText registerPasswordConfirmation;
    @BindView(R.id.btn_register)
    Button registerButton;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, rootView);

        progressDialog = new ProgressDialog(getActivity());

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    createAccount();
                }
            }
        });


        return rootView;
    }

    private boolean validate(){
        boolean valid = true;

        return valid;
    }

    private void createAccount(){

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.authenticating));
        progressDialog.show();

        Call<User> call = Api.getClient().register(registerName.getText().toString(),
                registerEmail.getText().toString(),
                registerPassword.getText().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Response<User> response, Retrofit retrofit) {
                dismissProgressBar();
                if (response.isSuccess()) {
                    // get authentication token
                    final String token = response.headers().get(MyApplication.TOKEN_HEADER);
                    User user = response.body();
                    finishLogin(token, user);

                } else {
                    ResponseBody responseInfo = response.errorBody();
                    // handleApiError(response.code(), responseInfo);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                dismissProgressBar();
                //  onRequestFailed(null);
            }
        });
    }

    public void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void finishLogin(String token, User user){
        //Save the token
        SharedPreferences.Editor editor = getContext()
                .getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE)
                .edit();
        Log.v("GL::","Bearer " + token);
        editor.putString(MyApplication.TOKEN, "Bearer " + token);
        editor.putLong(MyApplication.USER_ID, user.getId());
        editor.putString(MyApplication.USER_AVATAR, user.getPicture());
        editor.putString(MyApplication.USER_EMAIL, user.getEmail());
        editor.apply();

        dismissProgressBar();

        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissProgressBar();
    }
}

