package br.com.openbus.openbusmapper.ui.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.listeners.OnItemClickListener;
import br.com.openbus.openbusmapper.models.Trip;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Guilherme on 24/09/2016.
 */
public class TripAdapter extends RecyclerView.Adapter<TripAdapter.ItemViewHolder> {

    private List<Trip> list = Collections.emptyList();
    private Context context;
    public Trip mSelectedTrip = null;

    public Trip getSelectedTrip() {
        return mSelectedTrip;
    }

    public void setSelectedTrip(Trip selectedTrip) {
        this.mSelectedTrip = selectedTrip;
    }

    public TripAdapter(Context context, List<Trip> list) {
        this.list = list;
        this.context = context;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.trip_name)
        TextView tripNameTv;
        @BindView(R.id.trip_return_name)
        TextView tripReturnName;
        @BindView(R.id.trip_via)
        TextView tripVia;
        @BindView(R.id.trip_radio)
        AppCompatRadioButton tripRadio;

        private OnItemClickListener itemClickListener;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(OnItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getLayoutPosition(), false);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_trip_pick, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        final Trip trip = list.get(position);
        holder.tripNameTv.setText(trip.getTripHeadsign());
        holder.tripReturnName.setText(trip.getHeadsignReturn());

        if(trip.getVia() != null){
            holder.tripVia.setText(" - " + trip.getVia());
            holder.tripVia.setVisibility(View.VISIBLE);
        }else{
            holder.tripVia.setVisibility(View.GONE);
        }

        holder.tripRadio.setTag(trip.getId());
        if(mSelectedTrip != null) {
            holder.tripRadio.setChecked(trip.getId().equals(mSelectedTrip.getId()));
        }
        holder.tripRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long tripId = (Long) v.getTag();
                Trip selectedTrip = null;
                for(Trip trip : list){
                    if(!trip.getId().equals(tripId)) continue;
                    selectedTrip = trip;
                    break;
                }
                if(selectedTrip != null){
                    mSelectedTrip = selectedTrip;
                    notifyDataSetChanged();
                }
            }
        });

        holder.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(mSelectedTrip != null && mSelectedTrip.getId().equals(list.get(position).getId())){
                    mSelectedTrip = null;
                }else{
                    mSelectedTrip = list.get(position);
                }
                notifyDataSetChanged();
            }
        });
    }


    public Trip removeItem(int position) {
        final Trip model = list.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Trip model) {
        list.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Trip model = list.remove(fromPosition);
        list.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void animateTo(List<Trip> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Trip> newModels) {
        for (int i = list.size() - 1; i >= 0; i--) {
            final Trip model = list.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Trip> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Trip model = newModels.get(i);
            if (!list.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Trip> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Trip model = newModels.get(toPosition);
            final int fromPosition = list.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

}

