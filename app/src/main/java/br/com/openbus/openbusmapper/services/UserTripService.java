package br.com.openbus.openbusmapper.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.activities.MainActivity;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.models.OLatLng;
import br.com.openbus.openbusmapper.models.OLocation;

/**
 * Created by Guilherme on 15/10/2016.
 */
public class UserTripService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    private Location mLastLocation;
    private ArrayList<OLocation> mLocations = new ArrayList<>();
    private ArrayList<OLatLng> mWaypoints = new ArrayList<>();
    private GoogleApiClient mGoogleApiClient;
    private String userTripKey;
    private Long startedAt;
    private LocationRequest mLocationRequest;

    private NotificationManager mNM;

    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = R.string.local_service_started;

    @Override
    public void onCreate() {
        Log.v("GL::tripservice","onCreate");
        super.onCreate();
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        showNotification();
    }

    protected synchronized void buildGoogleApiClient() {
        Log.v("GL::tripservice","buildGoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void requestLocationUpdates(){
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //TODO: notify activity of permission requirements!
        } else {
            // permission has been granted, continue as usual
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(5000); //5 seconds
            locationRequest.setFastestInterval(1000); //1 seconds
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    locationRequest, this);
        }
    }

    @Override
    public boolean stopService(Intent name) {
        if(mGoogleApiClient != null){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        // Cancel the persistent notification.
        mNM.cancel(NOTIFICATION);
        reset();
        return super.stopService(name);
    }

    @Override
    public void onDestroy() {
        Log.v("GL::tripservice","onDestroy");
        super.onDestroy();
        if(mGoogleApiClient != null){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        // Cancel the persistent notification.
        mNM.cancel(NOTIFICATION);
        reset();
        stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v("GL::tripservice","onStartCommand, is intent null? "+String.valueOf(intent==null));
        if(intent != null && intent.getExtras() != null){
            startedAt = intent.getExtras().getLong("start");
            String newUserStripKey = intent.getExtras().getString("userTripKey");
            if(newUserStripKey != null && userTripKey != null && !userTripKey.equals(newUserStripKey)){
                reset();
            }
            userTripKey = newUserStripKey;
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }
        return Service.START_NOT_STICKY;//super.onStartCommand(intent, flags, startId);
    }

    public void reset(){
        mLocations = new ArrayList<>();
        mWaypoints = new ArrayList<>();
    }

    public void SendBroadcast() {
        Log.v("GL::tripservice","SendBroadcast");
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MyApplication.UPDATE_TRIP_INTENT);
        broadcastIntent.putExtra("locations", mLocations);
        broadcastIntent.putExtra("waypoints", mWaypoints);
        broadcastIntent.putExtra("lastLocation", mLastLocation);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.v("GL::tripservice","onConnected");
        requestLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("GL::tripservice","onConnectionSuspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v("GL::tripservice","onLocationChanged");
        if(System.currentTimeMillis() / 1000 - startedAt <= 5400){
            mLastLocation = location;
            mLocations.add(new OLocation(location));
            mWaypoints.add(new OLatLng(location.getLatitude(), location.getLongitude()));
            SendBroadcast();
        }else {
            stopSelf();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v("GL::tripservice","onConnectionFailed");
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = getText(R.string.local_service_started);

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        // Send the notification.
        Notification notification;
        if (Build.VERSION.SDK_INT < 16) {
            // do something for phones running an SDK before lollipop
            notification = new Notification.Builder(this)
                    .setContentTitle("Gravando trajeto").setContentText("Text")
                    .setSmallIcon(R.drawable.ic_bus).getNotification();
            mNM.notify(NOTIFICATION, notification);
        } else{
            // Do something for 16 and above versions
            // Set the info for the views that show in the notification panel.
            notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_bus)  // the status icon
                    .setTicker(text)  // the status text
                    .setWhen(System.currentTimeMillis())  // the time stamp
                    .setContentTitle("Gravando trajeto")  // the label of the entry
                    .setContentText(text)  // the contents of the entry
                    .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                    .build();
            mNM.notify(NOTIFICATION, notification);
        }

    }

    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        UserTripService getService() {
            return UserTripService.this;
        }
    }
}
