package br.com.openbus.openbusmapper.network;

import android.app.Activity;

import br.com.openbus.openbusmapper.utils.Helper;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by guilherme on 03/06/16.
 */
public abstract class RestCallback<T> implements Callback<T>{

    private Activity mActivity;

    public RestCallback(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onResponse(Response<T> response, Retrofit retrofit) {
        switch (response.code()){
            case 401:
            case 403:
                handle401();
                break;
        }
    }

    @Override
    public void onFailure(Throwable t) {
        t.printStackTrace();
    }

    private void handle401(){
        if(mActivity!=null){
            Helper.logout(mActivity, true);
        }
    }
}
