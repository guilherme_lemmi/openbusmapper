package br.com.openbus.openbusmapper.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.base.AppBaseActivity;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.db.Database;
import br.com.openbus.openbusmapper.fragments.dialogs.PickTripDialogFragment;
import br.com.openbus.openbusmapper.fragments.trips.TripReviewFragment;
import br.com.openbus.openbusmapper.listeners.OnTripPickedListener;
import br.com.openbus.openbusmapper.models.Campaign;
import br.com.openbus.openbusmapper.models.OLatLng;
import br.com.openbus.openbusmapper.models.OLocation;
import br.com.openbus.openbusmapper.models.Trip;
import br.com.openbus.openbusmapper.models.User;
import br.com.openbus.openbusmapper.models.UserTrip;
import br.com.openbus.openbusmapper.services.UserTripService;
import br.com.openbus.openbusmapper.utils.Helper;
import br.com.openbus.openbusmapper.utils.MapUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Guilherme on 09/10/2016.
 */
public class MainActivity extends AppBaseActivity
        implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, OnTripPickedListener {

    private static final int REQUEST_LOCATION = 110;
    private static final int REQUEST_RESOLVE_ERROR = 999;
    private static final int ACCESS_FINE_LOCATION_REQUEST = 555;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 0;
    private static final int LOCATION_SETTINGS_REQUEST_CODE = 1;

    private static final int STATE_INITIAL = 0;
    private static final int STATE_PREPARING_TO_RECORD_TRIP = 1;
    private static final int STATE_RECORDING_TRIP = 2;
    private static final int STATE_REVIEWING_TRIP = 3;
    private int currentState = STATE_INITIAL;

    private static final String TRIP_REVIEW_FRAGMENT = "TRIP_REVIEW_FRAGMENT";

    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;
    @BindView(R.id.fab_main)
    FloatingActionButton fabMain;
    @BindView(R.id.fab_secondary)
    FloatingActionButton fabSecondary;
    @BindView(R.id.add_stop_label)
    TextView addStopLabel;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.btn_send)
    TextView btnSend;
    @BindView(R.id.bottom_sheet_title)
    TextView bottomSheetTitle;
    @BindView(R.id.bottom_sheet_action_title)
    TextView bottomSheetActionTitle;
    @BindView(R.id.fab_secondary_container)
    RelativeLayout fabSecondaryContainer;
    @BindView(R.id.user_score_avatar)
    ImageView userScoreAvatar;

    /* trip panel*/
    @BindView(R.id.trip_headsign)
    TextView tripHeadsign;
    @BindView(R.id.trip_distance)
    TextView tripDistance;
    @BindView(R.id.trip_speed)
    TextView tripSpeed;
    @BindView(R.id.trip_stops)
    TextView tripStops;
    @BindView(R.id.trip_chronometer)
    Chronometer tripChronometer;
    @BindView(R.id.bottom_sheet_panel)
    LinearLayout bottomSheetPanel;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError;

    private Location mLastLocation;
    private Trip mSelectedTrip;
    private Campaign mSelectedCampaign;
    private UserTrip mUserTrip;
    private String mUserTripKey;
    private Long mChronoBase;
    private Intent mServiceIntent;

    private Marker mPositionMarker;
    private TripReviewFragment mTripReviewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        if(savedInstanceState != null){
            currentState = savedInstanceState.getInt("current_state");

            if(savedInstanceState.containsKey("current_key")){
                mUserTripKey = savedInstanceState.getString("current_key");
            }

            if(savedInstanceState.containsKey("current_trip")){
                mSelectedTrip = (Trip) savedInstanceState.getSerializable("current_trip");
                mSelectedCampaign = (Campaign) savedInstanceState.getSerializable("current_campaign");
            }

            if(savedInstanceState.containsKey("chrono_base_time")){
                mChronoBase = savedInstanceState.getLong("chrono_base_time");
            }

            if(savedInstanceState.containsKey("mTripReviewFragment")){
                mTripReviewFragment = (TripReviewFragment) getSupportFragmentManager()
                        .getFragment(savedInstanceState, "mTripReviewFragment");
            }

            if(mUserTripKey != null){
                Database db = MyApplication.getDb();
                mUserTrip = db.getUsertrip(getApplicationContext(), mUserTripKey);
            }else{
                mUserTrip = new UserTrip();
            }
            Log.v("GL::","onCreate, curstate is "+currentState);
        }
        buildGoogleApiClient();
        setupMapIfNeeded();
        setupUI();
        checkPlayServices();
        gpsActions();
    }

    /**
     * setup UI for the initial app state
     */
    private void initialState() {
        currentState = STATE_INITIAL;
        toggleMyLocation(true);

        if(mServiceIntent != null){
            LocalBroadcastManager.getInstance(this).unregisterReceiver(userTripReceiver);
            stopService(mServiceIntent);
            mServiceIntent = null;
        }

        requestLocationUpdates(LocationRequest.PRIORITY_LOW_POWER);

        if(mTripReviewFragment != null){
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction()
                    .remove(mTripReviewFragment).commit();
            mTripReviewFragment = null;
        }

        updateUI();

        mMap.clear();
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.setPadding(0, 60, 0, 0);

        if (mPositionMarker != null) {
            mPositionMarker.remove();
            mPositionMarker = null;
        }

        if (mLastLocation != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                    .zoom(18.0f)
                    .tilt(0)
                    .bearing(0)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    /**
     * Setup UI for preparing to record trip state
     */
    private void prepareToRecordTrip() {
        currentState = STATE_PREPARING_TO_RECORD_TRIP;
        toggleMyLocation(false);
        requestLocationUpdates(LocationRequest.PRIORITY_HIGH_ACCURACY);

        updateUI();
        bottomSheetPanel.setVisibility(View.VISIBLE);

        tripHeadsign.setText(mSelectedTrip.getTripHeadsign());
        tripStops.setText("0");
        tripDistance.setText("0 m");
        tripSpeed.setText("0 km/h");

        mMap.setPadding(0, 60, 0, 150);
        mMap.getUiSettings().setScrollGesturesEnabled(false);

        if (mLastLocation != null) {
            LatLng center = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(center)
                    .zoom(19.5f)
                    .tilt(60)
                    .bearing(mLastLocation.getBearing())
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            MarkerOptions options = new MarkerOptions()
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ic_navigation_black_48dp))
                    .anchor(0.5f, 0.5f)
                    .rotation(mLastLocation.getBearing())
                    .flat(true)
                    .position(center);
            mPositionMarker = mMap.addMarker(options);
        }

    }

    /**
     * setup UI for trip recording state.
     * Initiates UserTripService and listen for updates
     */
    private void recordTrip(boolean fromState) {
        currentState = STATE_RECORDING_TRIP;

        if(!fromState){
            mUserTrip = new UserTrip(mSelectedTrip,
                    new User(Helper.getCurrentUserId(getApplicationContext())));
            mUserTrip.setCampaign(mSelectedCampaign);
            Long time = System.currentTimeMillis() / 1000;
            mUserTrip.setStart(time);
            mUserTrip.setLastTime(time);
            mChronoBase = SystemClock.elapsedRealtime();

            Database db = MyApplication.getDb();
            String userKey = "user_trips:" + Helper.getCurrentUserId(getApplicationContext())+":";
            String[] keys = db.findKeys(getApplicationContext(), userKey);
            mUserTripKey = userKey + String.format("%05d", keys.length + 1);
            db.saveUserTrip(getApplicationContext(), mUserTripKey, mUserTrip);

            mServiceIntent = new Intent(this, UserTripService.class);
            mServiceIntent.putExtra("userTripKey",mUserTripKey);
            mServiceIntent.putExtra("start",time);
            startService(mServiceIntent);

        } else {
            tripHeadsign.setText(mUserTrip.getTrip().getTripHeadsign());
            if(mUserTrip.getStopsData() != null && mUserTrip.getStopsData().size() > 0){
                tripStops.setText(String.valueOf(mUserTrip.getStopsData().size()));
            }
            tripDistance.setText((int) mUserTrip.getDistance().doubleValue() + " m");
        }

        IntentFilter filter = new IntentFilter(MyApplication.UPDATE_TRIP_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(userTripReceiver, filter);

        tripChronometer.setBase(mChronoBase);
        tripChronometer.start();
        updateUI();

    }

    private void stopTrip()  {
        tripChronometer.stop();
    }

    private void updateTripPanel(){
        if(mLastLocation.getSpeed() != 0){
            double speedKm = MapUtils.convertMpsToKph(mLastLocation.getSpeed());
            tripSpeed.setText((int) speedKm + " km/h");
        } else {
            Long duration = System.currentTimeMillis() / 1000 - mUserTrip.getLastTime();
            double speed = mUserTrip.getLastDistance() / duration;
            tripSpeed.setText((int) MapUtils.convertMpsToKph(speed) + " km/h");
        }
        tripDistance.setText(String.valueOf((int) mUserTrip.getDistance().doubleValue())+" m");
        if(mUserTrip != null && mUserTrip.getStopsData().size() > 0){
            tripStops.setText(String.valueOf(mUserTrip.getStopsData().size()));
        }
    }

    /**
     * setup UI for the reviewTrip state
     * saves trip to local DB
     */
    private void reviewTrip() {
        currentState = STATE_REVIEWING_TRIP;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(userTripReceiver);
        stopService(mServiceIntent);
        mServiceIntent = null;

        updateUI();

        mUserTrip.setUserTripKey(mUserTripKey);
        mUserTrip.setEnd(System.currentTimeMillis() / 1000);
        stopTrip();


        Database db = MyApplication.getDb();
        db.saveUserTrip(getApplicationContext(), mUserTripKey, mUserTrip);

        if (mPositionMarker != null) {
            mPositionMarker.remove();
            mPositionMarker = null;
        }

        bottomSheetPanel.setVisibility(View.GONE);
        FragmentManager fm = getSupportFragmentManager();
        if(mTripReviewFragment == null){
            Bundle args = new Bundle();
            args.putSerializable(MyApplication.USER_TRIP, mUserTrip);
            mTripReviewFragment = new TripReviewFragment();
            mTripReviewFragment.setArguments(args);
            fm.beginTransaction()
                    .add(R.id.trip_review_fragment_container,
                            mTripReviewFragment, TRIP_REVIEW_FRAGMENT).commit();
        }

        mSelectedCampaign = null;
        mSelectedTrip = null;
        mUserTripKey = null;
        mChronoBase = 0L;

        requestLocationUpdates(LocationRequest.PRIORITY_LOW_POWER);

    }

    @Override
    public void onLocationChanged(Location location) {
//        Log.v("GL::", "onLocationChanged");
        if(currentState == STATE_INITIAL){
            mLastLocation = location;
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                    .zoom(18.0f)
                    .tilt(0)
                    .bearing(0)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }else if(currentState == STATE_PREPARING_TO_RECORD_TRIP){
            mLastLocation = location;
            mMap.clear();
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                    .zoom(19.5f)
                    .tilt(60)
                    .bearing(location.getBearing())
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mPositionMarker = mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ic_navigation_black_48dp))
                    .anchor(0.5f, 0.5f)
                    .rotation(location.getBearing())
                    .flat(true)
                    .position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())));

        }
    }

    // Define the callback for user trip service
    private BroadcastReceiver userTripReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v("GL::","UserTripService update received");
            if(currentState != STATE_REVIEWING_TRIP){

                mMap.clear();

                Location newLocation = intent.getParcelableExtra("lastLocation");
                if(mLastLocation != null){
                    Double distance = SphericalUtil.computeDistanceBetween(
                            new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()),
                            new LatLng(newLocation.getLatitude(),newLocation.getLongitude()));
                    mUserTrip.setDistance(mUserTrip.getDistance() + distance);
                    mUserTrip.setLastDistance(distance);
                }
                mLastLocation = newLocation;

                List<OLocation> rawWaypoints = intent.getParcelableArrayListExtra("locations");
                List<OLatLng> waypoints = intent.getParcelableArrayListExtra("waypoints");
                mUserTrip.setRawData(rawWaypoints);
                mUserTrip.setData(waypoints);
                mUserTrip.setLastTime(System.currentTimeMillis() / 1000 - mUserTrip.getLastTime());

                updateTripPanel();

                if(mUserTrip.getStopsData() != null && mUserTrip.getStopsData().size() > 0){
                    for(OLatLng stopLatLng : mUserTrip.getStopsData()){
                        Marker stopMarker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(stopLatLng.getLatitude(), stopLatLng.getLongitude()))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.stop_confirmed))
                                .draggable(false));
                    }
                }

                mPositionMarker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_navigation_black_48dp))
                        .anchor(0.5f, 0.5f)
                        .rotation(mLastLocation.getBearing())
                        .flat(true)
                        .position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                        .zoom(19.5f)
                        .tilt(60)
                        .bearing(mLastLocation.getBearing())
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                //if(Helper.getConnectivityStatus(getApplicationContext()) != Helper.NOT_CONNECTED){
               //     new SnapToRoadsTask(MainActivity.this).execute(waypoints);
               // }else{
                    Log.v("GL::","going to print polyline "+waypoints.size());
                    printPolyline(waypoints);
            }
           // }
        }
    };

    /**
     * Adds bus stop to the current location
     */
    private void addStop() {
        Marker stopMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.stop_confirmed))
                .draggable(false));
        mUserTrip.getStopsData().add(new OLatLng(stopMarker.getPosition().latitude,
                stopMarker.getPosition().longitude));
        OLocation stopLocation = new OLocation(mLastLocation);
        stopLocation.setIsStop(true);
        mUserTrip.getRawData().add(stopLocation);
        Database db = MyApplication.getDb();
        db.saveUserTrip(getApplicationContext(), mUserTripKey, mUserTrip);
        updateTripPanel();

        MapUtils.dropPinEffect(stopMarker);
    }

    /**
     * Displays confirmation dialog when user leaves trip recording without finalizing it     *
     */
    private void showFinishTripConfirmationDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Atenção");
        alertDialog.setMessage("Deseja finalizar a gravação do trajeto?");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Não",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Sim",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        reviewTrip();
                    }
                });
        alertDialog.show();
    }

    /**
     * Displays dialog informing we need location permission
     */
    private void showLocationPermissionDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setMessage("A permissão de localização é necessária para o " +
                "funcionamento do aplicativo. Deseja editar suas permissões?");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Não",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Sim",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alertDialog.show();
    }

    /**
     * Display trip selection dialog
     */
    private void showPickTripDialog() {
        FragmentManager fm = getSupportFragmentManager();
        PickTripDialogFragment mDialog = new PickTripDialogFragment();
        mDialog.setOnTripPickedListener(this);
        mDialog.show(fm, "PICK_TRIP_DIALOG");
    }

    @Override
    public void onTripPicked(Trip trip, Campaign campaign) {
        if (trip == null || campaign == null) return;
        mSelectedTrip = trip;
        mSelectedCampaign = campaign;
        prepareToRecordTrip();
    }

    private Polyline printPolyline(List<OLatLng> parsedWaypoints) {
        List<LatLng> latLngs = new ArrayList<>();
        for (OLatLng oLatLng : parsedWaypoints){
            latLngs.add(new LatLng(oLatLng.getLatitude(), oLatLng.getLongitude()));
        }
        PolylineOptions options = new PolylineOptions().width(5).color(Color.RED).geodesic(false);
        if (parsedWaypoints != null && parsedWaypoints.size() > 0) {
            Log.v("GL::", "printing polyline, size is " + parsedWaypoints.size());
            options.addAll(latLngs);
        }
        return mMap.addPolyline(options);
    }

    /**
     * Setups the initial UI, adding listeners and behaviors
     */
    private void setupUI() {
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                            Log.v("GL::","state collapsed");
                        if(currentState == STATE_REVIEWING_TRIP){
                            Log.v("GL::","still recording!");
                            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
//                        if (!(currentState == STATE_PREPARING_TO_RECORD_TRIP
//                                || currentState == STATE_RECORDING_TRIP)) {
//                            Log.v("GL::","going back to initial");
//                            initialState();
////                            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//                        }
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.v("GL::","state hidden");
                        initialState();
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.v("GL::","state dragging");
                        if(currentState == STATE_RECORDING_TRIP){
                            Log.v("GL::","still recording!");
                            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        } else if (currentState == STATE_REVIEWING_TRIP){
                            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.v("GL::","onslide "+slideOffset);
//                if (slideOffset > 0) {
//                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
            }
        });
        Glide.with(this)
                .load(Helper.getCurrentUserAvatar(this))
                .asBitmap()
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false)
                .centerCrop()
                .into(new BitmapImageViewTarget(userScoreAvatar) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(
                                        MainActivity.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        userScoreAvatar.setImageDrawable(circularBitmapDrawable);
                    }
                });
        fabMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (currentState) {
                    case STATE_INITIAL:
                        showPickTripDialog();
                        break;
                    case STATE_PREPARING_TO_RECORD_TRIP:
                        recordTrip(false);
                        break;
                    case STATE_RECORDING_TRIP:
                        Log.v("GL::","button clicked");
                        currentState = STATE_REVIEWING_TRIP;
                        Log.v("GL::","button clicked state is "+currentState);
                        reviewTrip();
                        break;
                }
            }
        });
        fabSecondary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (currentState) {
                    case STATE_RECORDING_TRIP:
                        addStop();
                        break;
                }
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initialState();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTrip();
            }
        });
//        updateUI();
    }

    private void sendTrip(){
        Log.v("GL::","send trip");
    }

    private void handleState(){
        Log.v("GL::","handleState is "+currentState);
        switch (currentState) {
            case STATE_INITIAL:
                initialState();
                break;
            case STATE_PREPARING_TO_RECORD_TRIP:
                prepareToRecordTrip();
                break;
            case STATE_RECORDING_TRIP:
                recordTrip(true);
                break;
            case STATE_REVIEWING_TRIP:
                reviewTrip();
                break;
        }
    }

    /**
     * Updates UI according to the current app state
     */
    private void updateUI() {
        Log.v("GL::","updateUi, curstate is "+currentState);
        btnBack.setVisibility(View.GONE);
        btnSend.setVisibility(View.GONE);
//        tripImg.setVisibility(View.GONE);
        switch (currentState) {
            case STATE_INITIAL:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                fabSecondaryContainer.setVisibility(View.GONE);
                bottomSheetActionTitle.setVisibility(View.GONE);
                fabMain.setIcon(R.drawable.ic_bus);
                fabMain.setColorNormalResId(R.color.primary);
                fabMain.setColorPressedResId(R.color.primaryDark);
                fabMain.setVisibility(View.VISIBLE);
                break;
            case STATE_PREPARING_TO_RECORD_TRIP:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bottomSheetTitle.setText(R.string.title_record_trip);
                bottomSheetActionTitle.setVisibility(View.VISIBLE);
                bottomSheetActionTitle.setText(getString(R.string.action_start));
                fabSecondaryContainer.setVisibility(View.GONE);
                fabMain.setIcon(R.drawable.ic_rec);
                fabMain.setColorNormalResId(R.color.accentWarning);
                fabMain.setColorPressedResId(R.color.accentWarningPressed);
                fabMain.setVisibility(View.VISIBLE);
                break;
            case STATE_RECORDING_TRIP:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bottomSheetTitle.setText(getString(R.string.recording));
                bottomSheetActionTitle.setText(getString(R.string.action_finish));
                bottomSheetActionTitle.setVisibility(View.VISIBLE);
                fabSecondaryContainer.setVisibility(View.VISIBLE);
                fabSecondary.setColorNormalResId(R.color.lightGray);
                fabSecondary.setColorPressedResId(R.color.mediumGray);
                fabSecondary.setIcon(R.drawable.ic_marker);
                fabMain.setIcon(R.drawable.ic_finish);
                fabMain.setColorNormalResId(R.color.accentSuccess);
                fabMain.setColorPressedResId(R.color.accentSuccess);
                fabMain.setVisibility(View.VISIBLE);
                break;
            case STATE_REVIEWING_TRIP:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                fabMain.setVisibility(View.GONE);
                fabSecondaryContainer.setVisibility(View.GONE);
                bottomSheetActionTitle.setText("");
                bottomSheetActionTitle.setVisibility(View.GONE);
                btnBack.setVisibility(View.VISIBLE);
                btnSend.setVisibility(View.VISIBLE);
                if(mUserTrip != null && mUserTrip.getTrip() != null){
                    bottomSheetTitle.setText(mUserTrip.getTrip().getTripHeadsign());
                }
                break;
        }
    }

    private void toggleMyLocation(boolean enable) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // TODO: Display UI and wait for user interaction
                Log.v("GL::", "oops");
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        ACCESS_FINE_LOCATION_REQUEST);
            }
        } else {
            mMap.setMyLocationEnabled(enable);
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                            mGoogleApiClient);
                    if (mLastLocation != null) {
//                        initialState();
                        mMap.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(mLastLocation.getLatitude(),
                                                mLastLocation.getLongitude()),
                                        18.0f));
                    }
                } catch (SecurityException se) {
                    se.printStackTrace();
                }
            } else {
                Helper.showMsg(this, getString(R.string.location_permission_required), true);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MainActivity.LOCATION_SETTINGS_REQUEST_CODE ||
                requestCode == MainActivity.PLAY_SERVICES_RESOLUTION_REQUEST){
            initialState();
        }
    }

    private void requestLocationUpdates(int priority) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // TODO: Display UI and wait for user interaction
                Log.v("GL::", "lacking location permission!");
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        ACCESS_FINE_LOCATION_REQUEST);
            }
        } else {
            // permission has been granted, continue as usual
            int interval = 30000;
            int fastestInterval = 15000;
            if(priority == LocationRequest.PRIORITY_HIGH_ACCURACY){
                interval = 5000;
                fastestInterval = 1000;
            }
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(interval); //30 seconds
            locationRequest.setFastestInterval(fastestInterval); //15 seconds
            locationRequest.setPriority(priority);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    locationRequest,
                    this);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void setupMapIfNeeded() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        if (!mResolvingError) {
            if (result.hasResolution()) {
                try {
                    mResolvingError = true;
                    result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
                } catch (IntentSender.SendIntentException e) {
                    mGoogleApiClient.connect();
                }
            } else {
                Helper.showMsg(this,
                        "Erro: falha na conexão com Google Maps - " + result.getErrorCode(), true);
                mResolvingError = true;
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        initialState();
        handleState();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Helper.showMsg(this, "Erro: conexão interrompida", true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("GL::","onStart");
        Log.v("GL::","currentState is "+currentState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("GL::","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("GL::","onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.v("GL::","onRestart");
    }

    @Override
    public void onBackPressed() {
        if (currentState != STATE_INITIAL) {
            if(currentState == STATE_RECORDING_TRIP){
                showFinishTripConfirmationDialog();
            }else{
                initialState();
            }
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("current_state", currentState);

        if(mUserTripKey != null){
            savedInstanceState.putString("current_key", mUserTripKey);
        }

        if(mSelectedTrip != null){
            savedInstanceState.putSerializable("current_trip", mSelectedTrip);
            savedInstanceState.putSerializable("current_campaign", mSelectedCampaign);
        }

        if(mTripReviewFragment != null){
            getSupportFragmentManager().putFragment(savedInstanceState,
                    "mTripReviewFragment", mTripReviewFragment);
        }

        if(currentState == STATE_RECORDING_TRIP &&
                tripChronometer != null && tripChronometer.getBase() != 0){
            savedInstanceState.putLong("chrono_base_time", mChronoBase);
        }
    }


    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentState = savedInstanceState.getInt("current_state");

        if(savedInstanceState.containsKey("current_key")){
            mUserTripKey = savedInstanceState.getString("current_key");
        }

        if(savedInstanceState.containsKey("chrono_base_time")){
            mChronoBase = savedInstanceState.getLong("chrono_base_time");
        }

        if(savedInstanceState.containsKey("current_trip")){
            mSelectedTrip = (Trip) savedInstanceState.getSerializable("current_trip");
            mSelectedCampaign = (Campaign) savedInstanceState.getSerializable("current_campaign");
        }

        if(savedInstanceState.containsKey("mTripReviewFragment")){
            mTripReviewFragment = (TripReviewFragment) getSupportFragmentManager()
                    .getFragment(savedInstanceState, "mTripReviewFragment");
        }
    }

    @Override
    protected void onPause() {
        Log.v("GL::","onPause");
        super.onPause();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        if(currentState == STATE_RECORDING_TRIP){
            LocalBroadcastManager.getInstance(this).unregisterReceiver(userTripReceiver);
        }
    }

    @Override
    protected void onResume() {
        Log.v("GL::","onResume");
        super.onResume();
        Log.v("GL::","currentState is "+currentState);
        setupMapIfNeeded();
        mGoogleApiClient.connect();
        if(currentState == STATE_RECORDING_TRIP){
            IntentFilter filter = new IntentFilter(MyApplication.UPDATE_TRIP_INTENT);
            LocalBroadcastManager.getInstance(this).registerReceiver(userTripReceiver, filter);
        }
//        handleState();
    }
}
