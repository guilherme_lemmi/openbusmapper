package br.com.openbus.openbusmapper.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.Collections;
import java.util.List;

import br.com.openbus.openbusmapper.R;
import br.com.openbus.openbusmapper.activities.TripReviewActivity;
import br.com.openbus.openbusmapper.base.MyApplication;
import br.com.openbus.openbusmapper.listeners.OnItemClickListener;
import br.com.openbus.openbusmapper.models.UserTrip;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Guilherme on 24/09/2016.
 */
public class UserTripAdapter extends RecyclerView.Adapter<UserTripAdapter.ItemViewHolder> {

    private List<UserTrip> list = Collections.emptyList();
    private Context context;

    public UserTripAdapter(Context context, List<UserTrip> list) {
        this.list = list;
        this.context = context;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.trip_img)
        ImageView tripImg;
        @BindView(R.id.trip_name)
        TextView tripName;
        @BindView(R.id.campaign_name)
        TextView campaignName;
        @BindView(R.id.trip_date)
        TextView tripDate;

        private OnItemClickListener itemClickListener;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(OnItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getLayoutPosition(), false);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_trip, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        final UserTrip userTrip = list.get(position);
        holder.tripName.setText(userTrip.getTrip().getTripHeadsign());
        holder.tripDate.setText(String.valueOf(userTrip.getStart()));
        holder.campaignName.setText(userTrip.getCampaign().getName());
        if(userTrip.getImgFile() != null){
            File file = new File(userTrip.getImgFile());
            Uri imageUri = Uri.fromFile(file);
            Glide.with(context)
                    .load(imageUri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(false)
                    .centerCrop()
                    .into(holder.tripImg);
        }

        holder.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                UserTrip clickedTrip = list.get(position);
                Intent i = new Intent(context, TripReviewActivity.class);
                i.putExtra(MyApplication.USER_TRIP_KEY, clickedTrip.getUserTripKey());
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

}

