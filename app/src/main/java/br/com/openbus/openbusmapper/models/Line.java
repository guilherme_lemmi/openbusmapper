package br.com.openbus.openbusmapper.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Guilherme on 24/09/2016.
 */
public class Line {

    @Expose
    Long id;

    @Expose
    String headsign;

    @Expose
    @SerializedName("headsign_return")
    String headsignReturn;

    @Expose
    String via;

    public Line() {
    }

    public Line(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

    public String getHeadsignReturn() {
        return headsignReturn;
    }

    public void setHeadsignReturn(String headsignReturn) {
        this.headsignReturn = headsignReturn;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }
}
