package br.com.openbus.openbusmapper.base;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import br.com.openbus.openbusmapper.db.Database;
import br.com.openbus.openbusmapper.models.User;
import br.com.openbus.openbusmapper.network.Api;
import br.com.openbus.openbusmapper.utils.Helper;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Guilherme on 09/10/2016.
 */
public class MyApplication extends Application {

    public static final String TAG = MyApplication.class.getSimpleName();
    public static final String USER = "USER";
    public static final String TRIP = "TRIP";
    public static final String USER_TRIP = "USER_TRIP";
    public static final String USER_TRIP_KEY = "USER_TRIP_KEY";
    public static final String TOKEN = "USER_TOKEN";
    public static final String TOKEN_EXPIRED = "TOKEN_EXPIRED";
    public static final String ACCOUNT_DISABLED = "ACCOUNT_DISABLED";
    public static final String USER_ID = "USER_ID";
    public static final String USER_AVATAR = "USER_AVATAR";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_CAMPAIGNS = "USER_CAMPAIGNS";
    public static final String TOKEN_HEADER = "Authorization";
    public static final String UPDATE_TRIP_INTENT = "br.com.openbus.openbusmapper.UPDATE_TRIP";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy - kk:mm";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String TIME_FORMAT = "kk:mm";
    public static final String NOTIFICATION_MESSAGE = "NOTIFICATION_MESSAGE";
    public static final String NOTIFICATION_ESTIMATE_ID = "NOTIFICATION_ESTIMATE_ID";
    public static final String GCM_SENT_TOKEN = "GCM_SENT_TOKEN";
    public static final String GCM_REGISTRATION_COMPLETE = "GCM_REGISTRATION_COMPLETE";
    private static final String DATABASE_NAME = "OpenBusMapper";

    private static Database db;

    public MyApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        validateToken();
        db = new Database(getApplicationContext(), DATABASE_NAME);
    }

    private void validateToken(){
        final String token = Helper.getCurrentToken(getApplicationContext());
        if(token != null){
            Long userId = Helper.getCurrentUserId(getApplicationContext());
            Call<User> call = Api.getClient().getCurrentUser(token, userId);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Response<User> response, Retrofit retrofit) {
                    Log.v(TAG, "response is " + response.raw());

                    SharedPreferences.Editor editor = getApplicationContext()
                            .getSharedPreferences(MyApplication.USER, Context.MODE_PRIVATE)
                            .edit();

                    if (!response.isSuccess() && response.code() != 200) {
                        editor.remove(MyApplication.TOKEN)
                                .remove(MyApplication.USER_ID)
                                .remove(MyApplication.USER_EMAIL)
                                .apply();
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    public static Database getDb() {
        return db;
    }

    public void setDb(Database db) {
        this.db = db;
    }
}
