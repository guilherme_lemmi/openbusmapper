package br.com.openbus.openbusmapper.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Guilherme on 16/10/2016.
 */
public class OLatLng implements Parcelable, Serializable{

    @Expose
    private Double latitude;

    @Expose
    private Double longitude;

    public OLatLng() {
        this.latitude = 0D;
        this.longitude = 0D;
    }

    public OLatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public OLatLng(Parcel in){
        double[] data = new double[2];
        in.readDoubleArray(data);
        this.latitude = data[0];
        this.longitude = data[1];
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDoubleArray(new double[] {this.latitude,
                this.longitude});
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public OLatLng createFromParcel(Parcel in) {
            return new OLatLng(in);
        }

        public OLatLng[] newArray(int size) {
            return new OLatLng[size];
        }
    };

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
